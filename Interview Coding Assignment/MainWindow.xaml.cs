﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interview_Coding_Assignment.Code;
using Microsoft.Win32;

namespace Interview_Coding_Assignment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SelectFileButton_Click(object sender, RoutedEventArgs e)
        {
            var fileName = SelectFile();

            if (fileName == "")
                return;

            var values = ReadValuesFromFile(fileName);            
        }
       
        private string SelectFile()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            return openFileDialog.FileName;
        }

        private IEnumerable<string> ReadValuesFromFile(string fileName)
        {
            var fileValuesReader = new FileValuesReader(fileName);
            var values = new StoredValues(fileValuesReader);
            return values.GetValues();
        }


    }
}

﻿using System.Collections.Generic;
using Interview_Coding_Assignment.Interfaces;

namespace Interview_Coding_Assignment.Code
{
    public class StoredValues
    {
        private readonly IValuesReader _valuesReader;

        public StoredValues(IValuesReader valuesReader)
        {
            _valuesReader = valuesReader;
        }

        /// <summary>
        /// The function returns collection of values
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetValues()
        {
            return _valuesReader.GetValues();
        }

    }
}

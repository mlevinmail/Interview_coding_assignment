﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using Interview_Coding_Assignment.Interfaces;

namespace Interview_Coding_Assignment.Code
{
    internal class FileValuesReader : IValuesReader
    {
   
        private readonly string _fileName;
        private readonly List<string> _values = new List<string>();
        public FileValuesReader(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new IOException("File does not exist");
            }

            _fileName = fileName;
        }

        public IEnumerable<string> GetValues()
        {
            var line = "";
            var file = new StreamReader(_fileName);
            while ((line = file.ReadLine()) != null)
            {
                _values.Add(line);
            }

            return _values;
        }
    }
}
